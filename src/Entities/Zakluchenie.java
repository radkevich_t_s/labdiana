package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Zakluchenie {
    private int idZakluchenia;
    private String text;
    private String date;

    public Zakluchenie() {
    }

    public Zakluchenie(String date, String text, int idZakluchenia) {
        this.date = date;
        this.text = text;
        this.idZakluchenia = idZakluchenia;
    }

    public int getIdZakluchenia() {
        return idZakluchenia;
    }

    public void setIdZakluchenia(int idZakluchenia) {
        this.idZakluchenia = idZakluchenia;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
