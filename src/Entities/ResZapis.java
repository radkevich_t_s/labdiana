package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class ResZapis {
    private int idZapis;
    private String nameZapis;
    private String date;
    private int id_patient;
    private int id_doctor;

    public ResZapis() {
    }

    public ResZapis(int idZapis, String nameZapis, String date, int id_patient, int id_doctor) {
        this.idZapis = idZapis;
        this.nameZapis = nameZapis;
        this.date = date;
        this.id_patient = id_patient;
        this.id_doctor = id_doctor;
    }

    public int getIdZapis() {
        return idZapis;
    }

    public void setIdZapis(int idZapis) {
        this.idZapis = idZapis;
    }

    public String getNameZapis() {
        return nameZapis;
    }

    public void setNameZapis(String nameZapis) {
        this.nameZapis = nameZapis;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public int getId_doctor() {
        return id_doctor;
    }

    public void setId_doctor(int id_doctor) {
        this.id_doctor = id_doctor;
    }
}
