package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Doctor {

    private int idDoctor;
    private String name;

    public int getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(int idDoctor) {
        this.idDoctor = idDoctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Doctor() {
    }

    public Doctor(int idDoctor, String name) {
        this.idDoctor = idDoctor;
        this.name = name;
    }
}
