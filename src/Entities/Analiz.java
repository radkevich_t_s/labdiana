package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Analiz {
    private int idAnaliz;
    private String analizType;

    public Analiz(int idAnaliz, String analizType) {
        this.idAnaliz = idAnaliz;
        this.analizType = analizType;

    }

    public Analiz() {
    }

    public int getIdAnaliz() {
        return idAnaliz;
    }

    public void setIdAnaliz(int idAnaliz) {
        this.idAnaliz = idAnaliz;
    }

    public String getAnalizType() {
        return analizType;
    }

    public void setAnalizType(String analizType) {
        this.analizType = analizType;
    }


}
