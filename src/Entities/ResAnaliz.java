package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class ResAnaliz {
    private int idAnaliz;
    private String nameAnaliz;
    private String date;
    private int id_patient;
    private int id_analiz;

    public int getIdAnaliz() {
        return idAnaliz;
    }

    public void setIdAnaliz(int idAnaliz) {
        this.idAnaliz = idAnaliz;
    }

    public String getNameAnaliz() {
        return nameAnaliz;
    }

    public void setNameAnaliz(String nameAnaliz) {
        this.nameAnaliz = nameAnaliz;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId_patient() {
        return id_patient;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public int getId_analiz() {
        return id_analiz;
    }

    public void setId_analiz(int id_analiz) {
        this.id_analiz = id_analiz;
    }

    public ResAnaliz(int idAnaliz, String nameAnaliz, String date, int id_patient, int id_analiz) {
        this.idAnaliz = idAnaliz;
        this.nameAnaliz = nameAnaliz;
        this.date = date;
        this.id_patient = id_patient;
        this.id_analiz = id_analiz;
    }

    public ResAnaliz() {
    }
}
