package connect;

import java.sql.Connection;

/**
 * Created by Shcherbina on 26.10.2016.
 */

// Реализация паттерна Синглтон
public class MySQLConnect {
    private static MySQLConnect mySQLConnect=null;
    private MySQLConnect() {
    }

    public static MySQLConnect getInstance(){
       if(mySQLConnect == null){
           mySQLConnect = new MySQLConnect();
       }
        return mySQLConnect;
    }

    public static Connection getConnection () {
        return UtilConnection.getJDBCMySqlConnection();
    }
}
