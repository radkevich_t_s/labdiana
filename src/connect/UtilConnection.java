package connect;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;


/**
 * Created by Shcherbina on 21.09.2016.
 */
public class UtilConnection {

    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("setting");

    protected static Connection getJDBCMySqlConnection(){
        Properties properties = new Properties();

        String username = BUNDLE.getString("db.username");
        String password = BUNDLE.getString("db.password");
        properties.setProperty("useUnicode","true");
        properties.setProperty("characterEncoding","UTF-8");
        properties.setProperty("user",username);
        properties.setProperty("password",password);
        String driver = "com.mysql.jdbc.Driver";
        String url = BUNDLE.getString("db.url");
        ConnectProxy proxy = new ConnectProxy();
        return proxy.getConnect(url, properties, driver);
    }

    private UtilConnection() {
    }

    public ResourceBundle getBangle(){
        return BUNDLE;
    }
}
