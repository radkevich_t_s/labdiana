package connect;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by Tatsiana on 03.11.2016.
 */
public class ConnectProxy implements IConnect{
    @Override
    public Connection getConnect(String url, Properties properties, String driver) {
        Connection conn = null;
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, properties);
        }catch (Exception e){
            e.printStackTrace();
        }
        return conn;
    }
}
