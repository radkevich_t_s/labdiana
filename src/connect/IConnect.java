package connect;

import javax.swing.*;
import java.sql.Connection;
import java.util.Properties;

/**
 * Created by Tatsiana on 03.11.2016.
 */
public interface IConnect {
    public Connection getConnect(String url, Properties properties, String driver);
}
