package entities;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Patient {
    private int idPatient;
    private String name;

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient() {
    }

    public Patient(int idPatient, String name) {
        this.idPatient = idPatient;
        this.name = name;
    }
}
