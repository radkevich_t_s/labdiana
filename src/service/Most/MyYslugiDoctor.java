package service.Most;

/**
 * Created by Tatsiana on 03.11.2016.
 */
public class MyYslugiDoctor extends MyYslugi{

    @Override
    public String[] getYslugiOnThisDate(String date) {
        String[] doc = new String[3];
        doc[0] = "Терапевт";
        doc[1] = "Педиатор";
        doc[2] = "Лор";
        return doc;
    }

    @Override
    public String[] getAll() {
        String[] doc = new String[3];
        doc[0] = "Терапевт";
        doc[1] = "Педиатор";
        doc[2] = "Лор";
        return doc;
    }
}
