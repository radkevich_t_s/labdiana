package service.command;

import dao.AnalizDao;
import service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by  on 27.10.2016.
 */
public class ResAnaliz implements ICommand{

    @Override
    public void execute(int id_patient){
        System.out.println("Выберите анализ:");
        AnalizDao analizDao = new AnalizDao();
        List<entities.Analiz> analizs = null;
        try {
            analizs = analizDao.getAllAnaliz();
            for(entities.Analiz analiz:analizs){
                System.out.println(analiz.getAnalizType() + " - " + analiz.getIdAnaliz());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Scanner scan = new Scanner(System.in);
        int id_analiz = scan.nextInt();


        Result result = new Result();
        MedServiceResultBuilder medServiceResultBuilder = new MyMedServiseResult();
        result.setMedServiceResultBuilder(medServiceResultBuilder);
        result.ConstructResultA(id_analiz, id_patient);
    }

}
