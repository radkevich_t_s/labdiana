package service.command;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface ICommand {
    public void execute(int id);
}
