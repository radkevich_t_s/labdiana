package service.command;

import dao.DoctorDao;
import service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by  on 27.10.2016.
 */
public class ResDoctor implements ICommand{

    @Override
    public void execute(int id_patient){
        System.out.println("Выберите врача:");
        DoctorDao doctorDao = new DoctorDao();
        List<entities.Doctor> doctors = null;
        try {
            doctors = doctorDao.getAllDoctors();
            for(entities.Doctor doctor:doctors){
                System.out.println(doctor.getName() + " - " + doctor.getIdDoctor());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Scanner scan = new Scanner(System.in);
        int id_doctor = scan.nextInt();

        Result result = new Result();
        MedServiceResultBuilder medServiceResultBuilder = new MyMedServiseResult();
        result.setMedServiceResultBuilder(medServiceResultBuilder);
        result.ConstructResultD(id_doctor, id_patient);
    }

}
