package service.command;

import dao.AnalizDao;
import dao.DoctorDao;
import entities.Analiz;
import service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by  on 27.10.2016.
 */
public class Doctor implements ICommand{

    @Override
    public void execute(int id_patient){
        System.out.println("Выберите врача:");
        DoctorDao doctorDao = new DoctorDao();
        List<entities.Doctor> doctors = null;
        try {
             doctors = doctorDao.getAllDoctors();
            for(entities.Doctor doctor:doctors){
                System.out.println(doctor.getName() + " - " + doctor.getIdDoctor());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Scanner scan = new Scanner(System.in);
        int id_doctor = scan.nextInt();
        System.out.println("Введите дату посещения:");
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();

        Posechenie posechenie = new Posechenie();
        MedServiceZapisBuilder medServiceZapisBuilder = new MyMedServiceZapis();
        posechenie.setMedServiceZapisBuilder(medServiceZapisBuilder);
        posechenie.ConstructPosechenieD(id_doctor, id_patient, date);
    }

}
