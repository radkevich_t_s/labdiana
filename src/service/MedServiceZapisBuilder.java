package service;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public abstract class MedServiceZapisBuilder {
    protected MedService medService;

    public MedService getMedService(){
        return medService;
    }

    public void createNewMedService(){
        medService = new MedService();
    }

    public abstract void buidZapisNaPriem(int doctor, int id_patient, String time);
    public abstract void buidZapisNaAnaliz(int analiz, int id_patient,  String time);
}
