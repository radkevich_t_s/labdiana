package service;

import service.MedService;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Posechenie {
    private MedServiceZapisBuilder medServiceZapisBuilder;

    public void setMedServiceZapisBuilder(MedServiceZapisBuilder medServiceZapisBuilder) {
        this.medServiceZapisBuilder = medServiceZapisBuilder;
    }

    public MedService getMedService(){
        return medServiceZapisBuilder.getMedService();
    }

    public void ConstructPosechenieAll(int doctor, String doctorTime, int analiz, String analizTime, int id_patient){
        medServiceZapisBuilder.buidZapisNaPriem(doctor, id_patient, doctorTime);
        medServiceZapisBuilder.buidZapisNaAnaliz(analiz, id_patient, analizTime);
    }

    public void ConstructPosechenieA( int analiz, int id_patient, String analizTime){
        medServiceZapisBuilder.buidZapisNaAnaliz(analiz, id_patient, analizTime);
    }

    public void ConstructPosechenieD(int doctor, int id_patient, String doctorTime){
        medServiceZapisBuilder.buidZapisNaPriem(doctor, id_patient, doctorTime);
    }
}
