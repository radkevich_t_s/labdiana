package service;

import dao.ResaltAnalizDao;
import dao.ResaltZapisDao;
import entities.ResZapis;

import java.sql.SQLException;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class MyMedServiceZapis extends MedServiceZapisBuilder{

    @Override
    public void buidZapisNaPriem(int doctor, int id_patient, String time) {
        ResaltZapisDao resaltZapisDao = new ResaltZapisDao();
        try {
            System.out.println(resaltZapisDao.setZapis(doctor, id_patient, time));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void buidZapisNaAnaliz(int analiz, int id_patient, String time) {

        ResaltAnalizDao resaltAnalizDao = new ResaltAnalizDao();
        try {
            System.out.println(resaltAnalizDao.setZapis(analiz, id_patient, time));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
