package service;

import entities.*;

import java.util.List;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public  class MedService {

    private Zakluchenie zakluchenie = null;
    private Doctor doctor = null;
    private Analiz analiz = null;
    private ResZapis resZapis = null;
    private ResAnaliz resAnaliz = null;

    public void setZakluchenie(Zakluchenie zakluchenie) {
        this.zakluchenie = zakluchenie;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public void setAnaliz(Analiz analiz) {
        this.analiz = analiz;
    }

    public void setResZapis(ResZapis resZapis) {
        this.resZapis = resZapis;
    }

    public void setResAnaliz(ResAnaliz resAnaliz) {
        this.resAnaliz = resAnaliz;
    }
}

