package service;

import entities.ResAnaliz;
import entities.ResZapis;
import service.MedService;
import service.MedServiceResultBuilder;

import java.util.List;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class Result {
    private MedServiceResultBuilder medServiceResultBuilder;

    public void setMedServiceResultBuilder(MedServiceResultBuilder medServiceResultBuilder) {
        this.medServiceResultBuilder = medServiceResultBuilder;
    }

    public MedService getMedService(){
        return medServiceResultBuilder.getMedService();
    }

    public void ConstructResultAll(int idA, int idDoc, int id_patient){
        medServiceResultBuilder.buidResultAnaliz(idA, id_patient);
        medServiceResultBuilder.buidResultPriem(idDoc, id_patient);
    }

    public void ConstructResultA(int idA, int id_patient){
        List<ResAnaliz> resAnalizs = medServiceResultBuilder.buidResultAnaliz(idA,id_patient);
        if(resAnalizs.size() != 0) {
            for (ResAnaliz resAnaliz : resAnalizs) {
                System.out.println(resAnaliz.getNameAnaliz() + " " + resAnaliz.getDate());
            }
        }else{
            System.out.println("вы не сдавали этот анализ");
        }
    }

    public void ConstructResultD(int idDoc, int id_patient){
        List<ResZapis> resZapises = medServiceResultBuilder.buidResultPriem(idDoc, id_patient);

        if(resZapises.size() != 0) {
            for (ResZapis resZapis : resZapises) {
                System.out.println(resZapis.getNameZapis() + " " + resZapis.getDate());
            }
        }else{
            System.out.println("Вы не посещали этого врача");
        }
    }
}
