package service;

import entities.ResZapis;
import service.MedService;

import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public abstract class MedServiceResultBuilder {
    protected MedService medService;

    public MedService getMedService(){
        return medService;
    }

    public void createNewMedService(){
        medService = new MedService();
    }

    public abstract List<ResZapis> buidResultPriem(int idPriem, int id_patient);
    public abstract List<entities.ResAnaliz> buidResultAnaliz(int idAnaliz, int id_patient);
}
