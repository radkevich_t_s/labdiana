package service;

import dao.ResaltAnalizDao;
import dao.ResaltZapisDao;
import entities.ResAnaliz;
import entities.ResZapis;
import entities.Zakluchenie;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class MyMedServiseResult extends MedServiceResultBuilder{
    @Override
    public List<ResZapis> buidResultPriem(int idPriem, int id_patient) {
        ResaltZapisDao resaltZapisDao = new ResaltZapisDao();
        try {
            return resaltZapisDao.getResZapisById(id_patient, idPriem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<ResAnaliz> buidResultAnaliz(int idAnaliz, int id_patient) {
        ResaltAnalizDao resaltAnalizDao = new ResaltAnalizDao();
        try{
            return resaltAnalizDao.getResAnalizById(id_patient, idAnaliz);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }
}
