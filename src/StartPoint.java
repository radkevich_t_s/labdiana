
import dao.PatientDao;
import entities.Patient;
import service.command.*;

import java.sql.SQLException;
import java.util.Locale;
import java.util.Scanner;

import static service.command.Service.*;

/**
 * Created by Shcherbina on 26.10.2016.
 */
public class StartPoint {
    public static void main (String[] args) throws SQLException {
        Locale.setDefault(new Locale("en"));
        System.out.println("Введите  имя:");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();

        PatientDao patientDao = new PatientDao();
        Patient patient = patientDao.getPatientByname(name);

        if(patient != null) {
            System.out.println("Ваши данные найдены");

            Analiz analiz = new Analiz();
            Doctor doctor = new Doctor();
            ResAnaliz resAnaliz = new ResAnaliz();
            ResDoctor resDoctor = new ResDoctor();
            Service service = new Service();

            boolean while_b = true;
            while (while_b) {
                System.out.println(name + ", выберите пункт:");
                System.out.println("Записаться на приём к терапевту - 1");
                System.out.println("Записаться на сдачу анализов - 2");
                System.out.println("Просмотреть результаты анализов - 3");
                System.out.println("Просмотреть результаты осмотра - 4");
                System.out.println("Распечатать заключение - 5");
                System.out.println("Выход - 6");
                int punct = scan.nextInt();
                switch (punct) {
                    case 1:
                        doctor.execute(patient.getIdPatient());
                        break;
                    case 2:
                        analiz.execute(patient.getIdPatient());
                        break;
                    case 3:
                        resAnaliz.execute(patient.getIdPatient());
                        break;
                    case 4:
                        resDoctor.execute(patient.getIdPatient());
                        break;
                    case 5:
                        service.execute(patient.getIdPatient());
                        break;
                    case 6:
                        while_b=false;
                        break;
                }
            }
        }else{
            System.out.println("Ивините, вас нет в системе");
        }
    }

}
