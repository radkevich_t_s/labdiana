package dao;

import connect.MySQLConnect;
import dao.idao.IPatientDao;
import entities.Patient;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 27.10.2016.
 */
public class PatientDao implements IPatientDao{

    @Override
    public List<Patient> getAllPatients() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from patient";
        List<Patient> patients = new ArrayList<>();

        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                Patient patient = new Patient(rs.getInt("id"), rs.getString("name"));
                patients.add(patient);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return patients;
    }

    @Override
    public Patient getPatientByname(String name) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT id, name from patient where name=\""+name+"\"";
        Patient patient = null;
        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                patient = new Patient(rs.getInt("id"), rs.getString("name"));
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return patient;
    }
}
