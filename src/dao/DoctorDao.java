package dao;

import connect.MySQLConnect;
import dao.idao.IDoctorDao;
import entities.Analiz;
import entities.Doctor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 27.10.2016.
 */
public class DoctorDao implements IDoctorDao{

    @Override
    public List<Doctor> getAllDoctors() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from doctor";
        List<Doctor> doctors = new ArrayList<>();

        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                Doctor doctor = new Doctor(rs.getInt("id"), rs.getString("name"));
                doctors.add(doctor);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return doctors;
    }

@Override
    public Doctor getDoctorById(int id) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT id, name from doctor where id="+id;
        Doctor doctor = null;
        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                doctor = new Doctor(rs.getInt("id"), rs.getString("name"));
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return doctor;
    }
}
