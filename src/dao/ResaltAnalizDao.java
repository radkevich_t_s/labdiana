package dao;

import connect.MySQLConnect;
import dao.idao.IResAnalizDao;
import entities.ResAnaliz;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 27.10.2016.
 */
public class ResaltAnalizDao implements IResAnalizDao{

    @Override
    public List<ResAnaliz> getAllResAnalizs() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from resultanaliz";
        List<ResAnaliz> resAnalizs = new ArrayList<>();

        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                ResAnaliz resAnaliz = new ResAnaliz(rs.getInt("id"), rs.getString("text"), rs.getString("date"), rs.getInt("id_analiz"),rs.getInt("id_patient"));
                resAnalizs.add(resAnaliz);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return resAnalizs;
    }

    @Override
    public List<ResAnaliz> getResAnalizById(int id_patient, int id_analiz) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from resultanaliz where id_patient="+id_patient + " and id_analiz="+id_analiz;

        List<ResAnaliz> resAnalizs = new ArrayList<>();
        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                ResAnaliz resAnaliz = new ResAnaliz(rs.getInt("id"), rs.getString("text"), rs.getString("date"), rs.getInt("id_analiz"),rs.getInt("id_patient"));
                resAnalizs.add(resAnaliz);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return resAnalizs;
    }

    @Override
    public String setZapis(int id_analiz, int id_patient, String date) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO resultanaliz (text, date, id_analiz, id_patient) VALUES (?,?,?,?)";

        try {
            dbConnection = MySQLConnect.getConnection();
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, "результат анализа хороший");
            preparedStatement.setString(2, date);
            preparedStatement.setInt(3,id_analiz);
            preparedStatement.setInt(4, id_patient);

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return "Вы записаны на приём";
    }
}
