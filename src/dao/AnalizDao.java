package dao;

import connect.MySQLConnect;
import dao.idao.IAnalizDao;
import entities.Analiz;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 27.10.2016.
 */
public class AnalizDao implements IAnalizDao{

    @Override
    public List<Analiz> getAllAnaliz() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from analiz";
        List<Analiz> analizs = new ArrayList<>();

        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                Analiz analiz = new Analiz(rs.getInt("id"), rs.getString("name"));
                analizs.add(analiz);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return analizs;
    }

    @Override
    public Analiz getAnalizById(int id) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT id, name from analiz where id="+id;
        Analiz analiz = null;
        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                analiz = new Analiz(rs.getInt("id"), rs.getString("name"));
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return analiz;
    }
}
