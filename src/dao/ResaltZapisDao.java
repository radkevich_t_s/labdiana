package dao;

import connect.MySQLConnect;
import dao.idao.IResZapisDao;
import entities.ResZapis;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 27.10.2016.
 */
public class ResaltZapisDao implements IResZapisDao{

    @Override
    public List<ResZapis> getAllResZapiss() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from resultdoctor";
        List<ResZapis> resZapiss = new ArrayList<>();

        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                ResZapis resZapis = new ResZapis(rs.getInt("id"), rs.getString("text"), rs.getString("date"), rs.getInt("id_doctor"),rs.getInt("id_patient"));
                resZapiss.add(resZapis);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return resZapiss;
    }

    @Override
    public List<ResZapis> getResZapisById(int id_patient, int id_doctor) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * from resultdoctor where id_patient="+id_patient + " and id_doctor="+id_doctor;

        List<ResZapis> resZapiss = new ArrayList<>();
        try {
            dbConnection = MySQLConnect.getConnection();
            statement = dbConnection.createStatement();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);

            while (rs.next()) {
                ResZapis resZapis = new ResZapis(rs.getInt("id"), rs.getString("text"), rs.getString("date"), rs.getInt("id_doctor"),rs.getInt("id_patient"));
                resZapiss.add(resZapis);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return null;

        } finally {

            if (statement != null) {
                statement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return resZapiss;
    }

    @Override
    public String setZapis(int id_doctor, int id_patient, String date) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO resultdoctor (text, date, id_doctor, id_patient) VALUES (?,?,?,?)";

        try {
            dbConnection = MySQLConnect.getConnection();
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, "Вы здоровы");
            preparedStatement.setString(2, date);
            preparedStatement.setInt(3,id_doctor);
            preparedStatement.setInt(4, id_patient);

            // execute insert SQL stetement
            preparedStatement.executeUpdate();

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {

            if (preparedStatement != null) {
                preparedStatement.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }

        }
        return "Вы записаны на приём";
    }
}
