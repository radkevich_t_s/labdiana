package dao.idao;

import connect.MySQLConnect;
import entities.Analiz;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface IAnalizDao {
    public List<Analiz> getAllAnaliz() throws SQLException;

    public Analiz getAnalizById(int id) throws SQLException;
}
