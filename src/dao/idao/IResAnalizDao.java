package dao.idao;

import connect.MySQLConnect;
import entities.ResAnaliz;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface IResAnalizDao {

    public List<ResAnaliz> getAllResAnalizs() throws SQLException;

    public List<ResAnaliz> getResAnalizById(int id_patient, int id_analiz) throws SQLException;

    public String setZapis(int id_analiz, int id_patient, String date) throws SQLException;
}
