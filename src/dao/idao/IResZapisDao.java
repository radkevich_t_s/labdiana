package dao.idao;

import connect.MySQLConnect;
import entities.ResZapis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface IResZapisDao {
    public List<ResZapis> getAllResZapiss() throws SQLException;

    public List<ResZapis> getResZapisById(int id_patient, int id_doctor) throws SQLException;

    public String setZapis(int id_doctor, int id_patient, String date) throws SQLException;
}
