package dao.idao;

import connect.MySQLConnect;
import entities.Patient;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface IPatientDao {
    public List<Patient> getAllPatients() throws SQLException;

    public Patient getPatientByname(String name) throws SQLException;
}
