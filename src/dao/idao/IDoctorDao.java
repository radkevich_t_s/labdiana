package dao.idao;

import connect.MySQLConnect;
import entities.Doctor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tatsiana on 02.11.2016.
 */
public interface IDoctorDao {
    public List<Doctor> getAllDoctors() throws SQLException;

    public Doctor getDoctorById(int id) throws SQLException;
}
