-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.11-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных med_serv
CREATE DATABASE IF NOT EXISTS `med_serv` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `med_serv`;


-- Дамп структуры для таблица med_serv.analiz
CREATE TABLE IF NOT EXISTS `analiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы med_serv.analiz: ~3 rows (приблизительно)
DELETE FROM `analiz`;
/*!40000 ALTER TABLE `analiz` DISABLE KEYS */;
INSERT INTO `analiz` (`id`, `name`) VALUES
	(1, 'Кровь общий'),
	(2, 'Кровь гормоны'),
	(3, 'Кровь биохимия');
/*!40000 ALTER TABLE `analiz` ENABLE KEYS */;


-- Дамп структуры для таблица med_serv.doctor
CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы med_serv.doctor: ~2 rows (приблизительно)
DELETE FROM `doctor`;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` (`id`, `name`) VALUES
	(1, 'Иванов Иван Иванович'),
	(2, 'Петров Пётр Петрович');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;


-- Дамп структуры для таблица med_serv.patient
CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы med_serv.patient: ~1 rows (приблизительно)
DELETE FROM `patient`;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` (`id`, `name`) VALUES
	(5, 'Diana');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;


-- Дамп структуры для таблица med_serv.resultanaliz
CREATE TABLE IF NOT EXISTS `resultanaliz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(12) NOT NULL DEFAULT '0',
  `id_analiz` int(11) NOT NULL DEFAULT '0',
  `id_patient` int(11) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK1_patient` (`id_patient`),
  KEY `FK2_analiz` (`id_analiz`),
  CONSTRAINT `FK1_patient` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id`),
  CONSTRAINT `FK2_analiz` FOREIGN KEY (`id_analiz`) REFERENCES `analiz` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы med_serv.resultanaliz: ~1 rows (приблизительно)
DELETE FROM `resultanaliz`;
/*!40000 ALTER TABLE `resultanaliz` DISABLE KEYS */;
INSERT INTO `resultanaliz` (`id`, `date`, `id_analiz`, `id_patient`, `text`) VALUES
	(2, '23.01.2015', 2, 5, 'результат анализа хороший');
/*!40000 ALTER TABLE `resultanaliz` ENABLE KEYS */;


-- Дамп структуры для таблица med_serv.resultdoctor
CREATE TABLE IF NOT EXISTS `resultdoctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(50) DEFAULT NULL,
  `date` varchar(150) DEFAULT NULL,
  `id_patient` int(11),
  `id_doctor` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `FK_patient` (`id_patient`),
  KEY `FK2_doctor` (`id_doctor`),
  CONSTRAINT `FK2_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `doctor` (`id`),
  CONSTRAINT `FK_patient` FOREIGN KEY (`id_patient`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы med_serv.resultdoctor: ~2 rows (приблизительно)
DELETE FROM `resultdoctor`;
/*!40000 ALTER TABLE `resultdoctor` DISABLE KEYS */;
INSERT INTO `resultdoctor` (`id`, `text`, `date`, `id_patient`, `id_doctor`) VALUES
	(1, 'Вы здоровы', '23.01.2016', 5, 1),
	(2, 'Вы здоровы', '26.05.2014', 5, 2);
/*!40000 ALTER TABLE `resultdoctor` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
